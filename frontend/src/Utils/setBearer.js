import service from "../apis";

const setBearer = (bearerToken) => {
  if (bearerToken) {
    service.defaults.headers.common["Authorization"] = `Bearer ${bearerToken}`;
  } else {
    delete service.defaults.headers.common["Authorization"];
  }
};

export default setBearer;
